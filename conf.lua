function love.conf (t)
    t.window.title = "Camera testing"
    t.window.width = 1024
    t.window.height = 768

    t.console = true
end