# Love2D-Camera

- *Move camera using mouse.*
- *Move camera using keyboard.*

![](img/love2d-camera.gif)

- `d` to show / hide debug information
- `escape` to exit
- `m` to hide custom cursor

To use the camera using the mouse, move it at MARGIN (actually 10 pixels) of the window

To use the camera with the keyboard, use the up / right / down / left keys